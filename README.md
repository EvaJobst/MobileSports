# Mountaineer

This app was developed for the university course "Mobile Sports". It is targeted at people that want to track their hiking tours. It features a list of all saved tours, settings that make the calculations more accurate and the ability to track your own tour. During the tour, the current location, time, speed, distance, weather and heart rate is displayed.

__Libraries:__ Firebase, GSON, Android Annotations, Eventbus, Retrofit
<br/><br/>



11/2017 - 01/2018 | Android Smartphones



Overview of all Tours            |  Settings | Started Tour
:-------------------------:|:-------------------------:|:----:
<img src="images/screenshots/app_screenshot_1.png" alt="Overview" width="200px">  |  <img src="images/screenshots/app_screenshot_2.png" alt="Details" width="200px"> | <img src="images/screenshots/app_screenshot_3.png" alt="Map" width="200px">


### Demonstration
 <img src="https://github.com/EvaJobst/MobileSports/blob/master/images/gifs/app_demo_small.gif" alt="Demo of App" height="400">
